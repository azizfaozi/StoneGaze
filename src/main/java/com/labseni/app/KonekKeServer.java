package com.labseni.app;
import java.io.IOException;
import java.net.InetAddress;

public class KonekKeServer {
	public static void main(String[] args)
	{
		//Print www.yahoo.com address details
		printAddressDetails("www.yahoo.com");
		//Print the loopback addres details
		printAddressDetails(null);
		//print the loopback address details
		printAddressDetails("::1");

	}
	public static void printAddressDetails(String host)
	{
		System.out.println("Host '"+host+"' details starting ...");
		try{
			InetAddress addr = InetAddress.getByName(host);
			System.out.println("Host IP address: "+addr.getHostAddress());
			System.out.println("Canonical Host Name: "+ addr.getCanonicalHostName());
			int timeOutinMillis=10000;
			System.out.println("isReachable():"+addr.isReachable(timeOutinMillis));
			System.out.println("isLoopBackAddress():"+addr.isLoopbackAddress());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally{
			System.out.println("Host '"+host+"'details ends...");
			System.out.println("");
		}
	}

}
